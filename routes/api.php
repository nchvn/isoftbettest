<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');

Route::group(['middleware' => 'jwt.auth'], function () {

    /*** TRANSACTIONS ***/

    Route::post('transaction', 'TransactionController@create');
    Route::get('transaction/{customerId}/{transactionId} ', 'TransactionController@get');
    Route::get('transactions', 'TransactionController@getAll');
    Route::post('transactions', 'TransactionController@getByFilter');
    Route::put('transaction/{transactionId} ', 'TransactionController@update');
    Route::delete('transaction/{transactionId} ', 'TransactionController@delete');

    /*** TRANSACTIONS ***/

    /*** CUSTOMERS ***/

    Route::post('customer', 'CustomerController@create');

    /*** CUSTOMERS ***/

});

