<?php

namespace App;

class Transaction extends BaseModel
{
    protected $fillable = ['amount', 'customerId'];

    /*** RELATIONS ***/


    public function customers()
    {
        return $this->belongsTo('App\Customer', 'customerId');
    }

    /*** RELATIONS ***/

    /*** SCOPES ***/

    public function scopeGetPrevDaySumTrans($query){
        return $query->whereDate('created_at', now()->subDay()->toDateString())->count();
    }

    /*** SCOPES ***/


}
