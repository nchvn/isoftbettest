<?php

namespace App;

class Customer extends BaseModel
{
    protected $fillable = ['name', 'cnp'];

    protected $hidden = ['cnp'];


    /*** RELATIONS ***/


    public function transactions()
    {
        return $this->hasMany('App\Transaction', 'customerId');
    }

    /*** RELATIONS ***/
}
