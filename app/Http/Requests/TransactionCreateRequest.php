<?php

namespace App\Http\Requests;

class TransactionCreateRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerId' => 'required|numeric|exists:customers,id',
            'amount' => ['required', 'regex:/^\d{0,8}(\.\d{1,2})?$/']
        ];
    }
}
