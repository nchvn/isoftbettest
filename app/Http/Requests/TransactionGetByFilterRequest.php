<?php

namespace App\Http\Requests;

class TransactionGetByFilterRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerId' => 'filled|numeric|exists:customers,id',
            'amount' => ['filled', 'regex:/^\d{0,8}(\.\d{1,2})?$/'],
            'date' => 'filled|in:Desc,Asc',
            'limit' => 'filled|integer'
        ];
    }
}
