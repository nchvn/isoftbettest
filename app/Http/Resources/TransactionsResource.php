<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'transactionId' => $this->id,
            'amount' => $this->amount,
            'date' => $this->created_at,
            'customerName' => $this->customers->name,
            'customerCnp' => $this->customers->cnp,
        ];
    }
}
