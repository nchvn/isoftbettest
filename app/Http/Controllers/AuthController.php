<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    public function register(RegisterRequest $request){
        $credentials = $request->only('email', 'password');
        $user = User::create($credentials);
        $token = auth()->login($user);
        return response()->json(['success' => true,
            'message' => trans('messages.success.registeredUser'),
            'user' => $user,
            'token' => $token]);
    }

    public function login(LoginRequest $request){
        $credentials = $request->only('email', 'password');
        try {
            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')]);
            }
        } catch (JWTException $e) {
            return $e->getMessage();
        }
        return response()->json(['success' => true, 'message' => trans('messages.success.login'), 'token' => $token]);
    }

}
