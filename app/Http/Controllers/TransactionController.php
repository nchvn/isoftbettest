<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\TransactionCreateRequest;
use App\Http\Requests\TransactionGetByFilterRequest;
use App\Http\Requests\TransactionUpdateRequest;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\TransactionsResource;
use App\Transaction;

class TransactionController extends Controller
{

    public function get($customerId, $transactionId){
        $customer = Customer::find($customerId);
        if($customer){
            $transaction = $customer->transactions->where('id', $transactionId)->first();
            if(!$transaction)
                return response()->json(['success' => false,
                    'message' => trans('messages.errors.dontHaveTransaction'),
                ]);
            return response()->json(['success' => true,
                'data' => new TransactionResource($transaction)]);
        }
        return response()->json(['success' => false,
            'message' => trans('messages.errors.dontExistCustomer'),
        ]);
    }

    public function getAll(){
        $transactions = Transaction::with('customers')->get();
        return response()->json(['success' => true,
            'data' => TransactionsResource::collection($transactions),
        ]);
    }

    public function getByFilter(TransactionGetByFilterRequest $request){
        $credentials = $request->except('date', 'limit');
        $date = $request->has('date') ? $request->date : 'desc';
        $transactions = Transaction::where($credentials)->orderBy('created_at', $date)->get();
        if($request->has('limit')){
            $transactions = $transactions->take($request->limit);
        }
        return response()->json(['success' => true,
            'data' => TransactionsResource::collection($transactions),
        ]);
    }

    public function create(TransactionCreateRequest $request){
        $credentials = $request->only('customerId', 'amount');
        $transaction = Transaction::create($credentials);
        return response()->json(['success' => true,
            'message' => trans('messages.success.createdTransaction'),
            'data' => new TransactionResource($transaction)
        ]);
    }

    public function update(TransactionUpdateRequest $request, $transactionId){
        $credentials = $request->all();
        $transaction = Transaction::find($transactionId);
        if($transaction){
            $transaction->update($credentials);
            return response()->json(['success' => true,
            'message' => trans('messages.success.updatedTransaction'),
            'data' => new TransactionResource($transaction)]);
        }
        return response()->json(['success' => false,
            'message' => trans('messages.errors.dontExistTransaction'),
        ]);
    }

    public function delete($transactionId){
        $transaction = Transaction::find($transactionId);
        if($transaction){
            $transaction->delete();
            return response()->json(['success' => true,
                'message' => trans('messages.success.deletedTransaction'),
                ]);
        }
        return response()->json(['success' => false,
            'message' => trans('messages.errors.dontExistTransaction'),
        ]);
    }
}
