<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerCreateRequest;
use App\Http\Resources\CustomerResource;
use App\Customer;

class CustomerController extends Controller
{
    public function create(CustomerCreateRequest $request){
        $credentials = $request->only('name', 'cnp');
        $user = Customer::create($credentials);
        return response()->json(['success' => true,
            'message' => trans('messages.success.createdUser'), 'data' => new CustomerResource($user)
        ]);
    }
}
