<?php

namespace App\Console\Commands;

use App\Report;
use App\Transaction;
use Illuminate\Console\Command;

class ReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:store';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command stores the sum of all transactions from previous day.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactionsCount = Transaction::getPrevDaySumTrans();
        Report::create($transactionsCount);
    }
}
