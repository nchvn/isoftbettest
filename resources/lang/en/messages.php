<?php
return [
    'errors' => [
        'cantFindAccount' => 'We cant find an account with this credentials.',
        'failedLogin' => 'Failed to login, please try again.',
        'dontHaveTransaction' => "This customer doesn't have this transaction.",
        'dontExistCustomer' => "This customer doesn't exist.",
        'dontExistTransaction' => "This transaction doesn't exist.",
    ],
    'success' => [
        'registeredUser' => 'User was successfully registered.',
        'createdUser' => 'User was successfully created.',
        'login' => 'You are logged in.',
        'createdTransaction' => 'Transaction was successfully created.',
        'updatedTransaction' => 'Transaction was successfully updated.',
        'deletedTransaction' => 'Transaction was successfully deleted.'
    ],
];