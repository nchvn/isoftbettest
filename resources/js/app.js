import VueRouter from "vue-router";
import Home from "./components/Home.vue";
import Login from "./components/Login.vue";
import Filter from "./components/Login.vue";
import VueFlashMessage from 'vue-flash-message';

require('./bootstrap');
require('vue-flash-message/dist/vue-flash-message.min.css');

window.Vue = require('vue');

Vue.component('login', Login);
Vue.component('home', Home);
Vue.use(VueRouter);
Vue.use(VueFlashMessage);

window.axios.interceptors.request.use(
    (config) => {
        let token = localStorage.getItem('token');

        if (token) {
            config.headers['Authorization'] = `Bearer ${ token }`;
        }

        return config;
    },

    (error) => {
        return Promise.reject(error);
    }
);

const routes = [
    { path: '/home', name: 'home',
        component: Home,
        beforeEnter: (to, from, next) => {
            if (!localStorage.getItem('token')) {
                return router.push({ name: 'login' });
            }
            return next();
        }
    },
    { path: '/', name: 'login', component: Login },
];

const router = new VueRouter({
    routes,
    mode: 'history'
});

const app = new Vue({
    el: '#app',
    router
});
