## Brief deployment manual 

1. Firstly clone project: git clone https://nchvn@bitbucket.org/nchvn/isoftbettest.git

2. After you clone project make this command in the terminal: composer update

3. Create localhost and database.

4. Rename .env.example file to .env and change for yourself.

5. Make these commands in the terminal:
 
 - php artisan key:generate
 
 - php artisan jwt:secret
 
 - php artisan migrate
 
 - npm install

##

After these steps you can go to your local host in browser.

Postman collection for testing API: https://www.getpostman.com/collections/b9291f2f8616861ec77d

In the Postman create your own environment for this project and add two global variables: 

- token

- host

In host field paste your local host and in token field paste token after auth requests.

##

For setting cron use this command: 47 23 */2 * * cd path/to/project && php artisan report:store >>/dev/null 2>&1




